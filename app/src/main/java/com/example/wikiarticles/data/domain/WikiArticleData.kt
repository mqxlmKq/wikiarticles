package com.example.wikiarticles.data.domain

import java.io.Serializable

data class WikiArticleData(val pageId: WikiPageId,
                           val title: String)

data class WikiPageId(val asInt: Int) : Serializable {
    init {
        require(asInt > 0)
    }
}