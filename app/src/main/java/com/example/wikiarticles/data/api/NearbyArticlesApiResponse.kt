package com.example.wikiarticles.data.api

class NearbyArticlesApiResponse(val query: Query) {

    class Query(val geosearch: List<Article>)

    /**
     * Note: unfortunately, wiki api does not provide images count for article. This part of task has been omitted.
     */
    class Article(val pageid: Int,
                  val title: String)
}