package com.example.wikiarticles.data.domain

data class LocationData(val latitude: Double,
                        val longitude: Double) {
    init {
        listOf(latitude, longitude).forEach {
            // The latitude and longitude must be in degrees.
            require(it in -180.0..180.0)
        }
    }
}

enum class LocationError {
    NO_PERMISSION,
    NO_LOCATION,
    CANCELLED,
    FAILED
}