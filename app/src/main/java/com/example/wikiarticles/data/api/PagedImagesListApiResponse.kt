package com.example.wikiarticles.data.api

class PagedImagesListApiResponse(val `continue`: Continue?,
                                 val query: Query) {

    class Continue(val imcontinue: String)

    class Query(val pages: Map<Int, Page>)

    class Page(val images: List<Image>,
               val title: String)

    class Image(val title: String)
}