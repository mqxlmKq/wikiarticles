package com.example.wikiarticles.data.mapping

import com.example.wikiarticles.data.api.NearbyArticlesApiResponse
import com.example.wikiarticles.data.api.PagedImagesListApiResponse
import com.example.wikiarticles.data.domain.WikiArticleData
import com.example.wikiarticles.data.domain.WikiArticleImageData
import com.example.wikiarticles.data.domain.WikiArticleImagesPageData
import com.example.wikiarticles.data.domain.WikiPageId

fun NearbyArticlesApiResponse.Article.toDomainData() = WikiArticleData(WikiPageId(pageid), title)

fun PagedImagesListApiResponse.Image.toDomainData() = WikiArticleImageData(title)

fun NearbyArticlesApiResponse.toWikiArticlesList() = query.geosearch.map { it.toDomainData() }

fun PagedImagesListApiResponse.toImagesPageData() = WikiArticleImagesPageData(
                                                        query.pages
                                                             .values
                                                             .singleOrNull()
                                                             ?.images
                                                             ?.map { it.toDomainData() }
                                                             .orEmpty(),

                                                        `continue`?.imcontinue
                                                    )