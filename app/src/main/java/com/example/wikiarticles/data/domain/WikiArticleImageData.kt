package com.example.wikiarticles.data.domain

data class WikiArticleImageData(val title: String)

data class WikiArticleImagesPageData(val images: List<WikiArticleImageData>,
                                     val continuationToken: String?)