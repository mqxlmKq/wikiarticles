package com.example.wikiarticles.navigation

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.example.wikiarticles.R
import com.example.wikiarticles.data.domain.WikiPageId
import com.example.wikiarticles.ui.articles.NearbyArticlesFragment
import com.example.wikiarticles.ui.images.ImagesListFragment

/**
 * This class encapsulate navigation between screens.
 */
class ScreensNavigator(private val fragmentManager: FragmentManager) {

    fun goToNearbyArticlesListScreen() {
        doFragmentTransactionIfStateNotSaved {
            add(R.id.container, NearbyArticlesFragment.newInstance())
        }
    }

    fun goToImagesListForArticleScreen(wikiPageId: WikiPageId) {
        doFragmentTransactionIfStateNotSaved {
            replace(R.id.container, ImagesListFragment.newInstance(wikiPageId))
            addToBackStack(null)
        }
    }

    private fun doFragmentTransactionIfStateNotSaved(configurator: FragmentTransaction.() -> Unit) {
        fragmentManager
            .takeUnless { it.isStateSaved }
            ?.beginTransaction()
            ?.also(configurator)
            ?.commit()
    }
}