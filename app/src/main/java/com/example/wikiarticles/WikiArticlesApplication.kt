package com.example.wikiarticles

import android.app.Application
import com.example.wikiarticles.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

@Suppress("unused")
class WikiArticlesApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@WikiArticlesApplication)
            modules(appModule)
        }
    }
}