package com.example.wikiarticles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.example.wikiarticles.navigation.ScreensNavigator

class MainActivity : AppCompatActivity(),
                     NavigationOwner {

    private lateinit var navigator: ScreensNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navigator = ScreensNavigator(supportFragmentManager)

        if (savedInstanceState == null) {
            navigator.goToNearbyArticlesListScreen()
        }
    }

    override fun getScreensNavigator(): ScreensNavigator = navigator
}

interface NavigationOwner {
    fun getScreensNavigator(): ScreensNavigator
}

fun Fragment.getScreensNavigator() = (activity as NavigationOwner?)?.getScreensNavigator()