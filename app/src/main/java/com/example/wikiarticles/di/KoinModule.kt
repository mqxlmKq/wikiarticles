package com.example.wikiarticles.di

import com.example.wikiarticles.api.WikiApiService
import com.example.wikiarticles.api.retrofit.WikiApi
import com.example.wikiarticles.api.retrofit.createRetrofit
import com.example.wikiarticles.location.LocationTracker
import com.example.wikiarticles.ui.articles.NearbyArticlesListViewModel
import com.example.wikiarticles.ui.images.ImagesListViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single {
        WikiApiService(createRetrofit().create(WikiApi::class.java))
    }

    single {
        LocationTracker(androidContext())
    }

    viewModel {
        NearbyArticlesListViewModel(get(), get())
    }

    viewModel { params ->
        ImagesListViewModel(get(), params[0])
    }
}