package com.example.wikiarticles.location

import android.annotation.SuppressLint
import android.content.Context
import com.example.wikiarticles.data.domain.LocationData
import com.example.wikiarticles.data.domain.LocationError
import com.example.wikiarticles.permissions.RuntimePermission
import com.example.wikiarticles.permissions.hasRuntimePermission
import com.google.android.gms.location.LocationServices
import java.lang.Exception
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine

/**
 * This class asynchronously retrieves last device's location.
 */
class LocationTracker(private val context: Context) {

    @SuppressLint("MissingPermission")
    suspend fun getLastLocation(): LocationData {
        return suspendCoroutine { continuation ->

            @SuppressLint("MissingPermission")
            if (context.hasRuntimePermission(RuntimePermission.LOCATION)) {
                LocationServices
                    .getFusedLocationProviderClient(context)
                    .lastLocation
                    .addOnSuccessListener { location ->
                        location
                            ?.let {
                                continuation.resume(
                                    LocationData(latitude = it.latitude,
                                                 longitude = it.longitude)
                                )

                            }
                            ?: continuation.resumeWithError(LocationError.NO_LOCATION)
                    }
                    .addOnCanceledListener {
                        continuation.resumeWithError(LocationError.CANCELLED)
                    }
                    .addOnFailureListener {
                        continuation.resumeWithError(LocationError.FAILED)
                    }
            } else {
                continuation.resumeWithError(LocationError.NO_PERMISSION)
            }
        }
    }

    private fun Continuation<LocationData>.resumeWithError(error: LocationError) {
        resumeWithException(LocationException(error))
    }
}

class LocationException(cause: LocationError): Exception()