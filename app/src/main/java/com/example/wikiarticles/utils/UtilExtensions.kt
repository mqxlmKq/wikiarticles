package com.example.wikiarticles.utils

import android.os.Bundle

fun <T: Any, U: Any> T?.pairValues(other: U?): Pair<T, U>? {
    return this?.let { first ->
               other?.let { second ->
                   first to second
               }
           }
}

inline fun <T> T?.orElse(lazyValue: () -> T): T = this ?: lazyValue()

@Suppress("UNCHECKED_CAST")
fun <T: Any> Bundle.getSerializableValue(key: String): T = getSerializable(key) as T