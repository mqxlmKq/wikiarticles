package com.example.wikiarticles.utils

/**
 * This class's purpose is to represent result or fail.
 *
 * It will have one of it's field to be not null - either result or fail.
 */
class ResultOrError<Result: Any, Error: Any> private constructor(val result: Result?,
                                                                 val error: Error?) {

    val isError: Boolean
        get() = error != null

    val isResult: Boolean
        get() = result != null

    init {
        require(   (error  == null && result != null)
                || (result == null && error  != null))
    }

    inline fun ifResult(crossinline consumer: (Result) -> Unit): ResultOrError<Result, Error> {
        result?.let(consumer)
        return this
    }

    inline fun ifError(crossinline consumer: (Error) -> Unit): ResultOrError<Result, Error> {
        error?.let(consumer)
        return this
    }

    override fun toString(): String {
        return if (error != null) {
            "Error: $error"
        } else {
            "Result: " + result!!
        }
    }

    // this is monadic bind for ResultOrError
    //
    // has type ResultOrError<R, E> -> (R -> ResultOrError<T, E>) -> ResultOrError<T, E>
    fun <T: Any> flatMap(action: (Result) -> ResultOrError<T, Error>): ResultOrError<T, Error>
            = if (result != null) action(result)
              else                ResultOrError(null, error)

    fun <NewResult: Any, NewError: Any> map(changeResult: (Result) -> NewResult,
                                            changeError:  (Error)  -> NewError): ResultOrError<NewResult, NewError>
            = if (result != null) ResultOrError(changeResult(result), null)
              else                ResultOrError(null,                 changeError(error!!))

    fun <R: Any> mapResult(action: (Result) -> R) = map(action, { it })

    fun <E: Any> mapError(action: (Error) -> E)   = map({ it }, action)


    fun resultOr(defaultValue: Result): Result = result ?: defaultValue

    fun resultOrThrow(createException: (Error) -> Throwable): Result {
        error?.let { throw createException(it) }
        return result!!
    }

    companion object {

        @JvmStatic
        fun <Result: Any, Error: Any> result(result: Result): ResultOrError<Result, Error> {
            return ResultOrError(result, null)
        }

        @JvmStatic
        fun <Result: Any, Error: Any> error(error: Error): ResultOrError<Result, Error> {
            return ResultOrError(null, error)
        }
    }
}

// ==============================================================================
// factory extensions
// ==============================================================================
fun <R: Any, E: Any> R.asResult() = ResultOrError.result<R, E>(this)

fun <R: Any, E: Any> E.asError() = ResultOrError.error<R, E>(this)

fun <R: Any, E: Any> R?.asResultOrError(error: E) = this?.asResult<R, E>() ?: error.asError()