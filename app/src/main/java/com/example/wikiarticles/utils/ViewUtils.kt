package com.example.wikiarticles.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment

fun View.setVisible() {
    visibility = View.VISIBLE
}

fun View.setGone() {
    visibility = View.GONE
}

fun Fragment.showLongToast(textResId: Int) {
    activity?.let {
        showLongToast(getString(textResId))
    }
}

fun Fragment.showLongToast(text: String) {
    activity?.let {
        Toast.makeText(it, text, Toast.LENGTH_LONG).show()
    }
}

fun Context.showLongToast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

fun Context.showLongToast(textResId: Int) {
    Toast.makeText(this, textResId, Toast.LENGTH_LONG).show()
}