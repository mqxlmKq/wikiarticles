package com.example.wikiarticles.utils

import android.os.Looper

fun assertUiThread() {
    val uiThread = Looper.getMainLooper().thread
    val currentThread = Thread.currentThread()

    if (uiThread !== currentThread) {
        throw RuntimeException("This call must be in UI thread")
    }
}