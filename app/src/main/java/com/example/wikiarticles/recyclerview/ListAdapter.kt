package com.example.wikiarticles.recyclerview

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.wikiarticles.ui.base.InflatingContext

/**
 * A complete single-type item RecyclerView adapter implementation based on composition approach.
 *
 * The client code defines a view holder class (based on RecyclerView.ViewHolder) and supplies holder
 * factory (createViewHolder argument) and holder binder (bindViewHolder argument).
 * Optionally, a click listener (applied for each item), update notifier (where client can use DiffUtil,
 * for example, to dispatch updates to RecyclerView) and getUniqueItemId (to enable adapter's stable ids features)
 * can be provided. If updateNotifier is not provided, notifyDataSetChanged() is used to dispatch updates.
 */
class ListAdapter<T : Any, VH : RecyclerView.ViewHolder>(
    private val createViewHolder: InflatingContext.() -> VH,
    private val bindViewHolder: VH.(T) -> Unit,
    private val itemClickListener: ItemClickListener<T>? = null,
    private val updateNotifier: UpdateNotifier<T> = { _, _, adapter -> adapter.notifyDataSetChanged() },
    private val getUniqueItemId: (T.() -> Long)? = null,
    initialDataList: List<T> = emptyList()
) : RecyclerView.Adapter<VH>() {

    var dataList: List<T> = initialDataList.toList()
        set(newData) {
            val oldData = field

            field = newData.toList()

            updateNotifier(oldData, newData, this)
        }

    init {
        setHasStableIds(getUniqueItemId != null)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        return InflatingContext(parent).createViewHolder()
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        val data = dataList[position]

        bindViewHolder(holder, data)

        itemClickListener?.let { listener ->
            holder.itemView.setOnClickListener { listener(position, data) }
        }
    }

    override fun getItemCount() = dataList.size

    override fun getItemId(position: Int): Long {
        return getUniqueItemId?.let { getId -> dataList[position].getId() }
            ?: super.getItemId(position)
    }
}

typealias ItemClickListener<T> = (position: Int, data: T) -> Unit
typealias UpdateNotifier<T> = (oldList: List<T>, newList: List<T>, adapter: ListAdapter<T, *>) -> Unit