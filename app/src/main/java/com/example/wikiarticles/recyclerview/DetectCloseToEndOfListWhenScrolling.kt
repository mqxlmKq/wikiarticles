package com.example.wikiarticles.recyclerview

import androidx.recyclerview.widget.RecyclerView

// Layout manager must be the layout manager that is set in the recycler view
class DetectCloseToEndOfListWhenScrolling(private val layoutManager: androidx.recyclerview.widget.LinearLayoutManager,
                                          private val onCloseToEnd: () -> Unit,
                                          private val itemsToConsiderClose: Int = 2) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: androidx.recyclerview.widget.RecyclerView, dx: Int, dy: Int)
    {
        super.onScrolled(recyclerView, dx, dy)

        // This code expects that layout manager that specified via it's
        // parameter is the same layout manager that is set in the recycler view
        if (recyclerView.layoutManager != layoutManager) throw RuntimeException("wrong recycler view")

        val totalItemCount = layoutManager.itemCount
        val lastVisibleItem = layoutManager.findLastVisibleItemPosition()

        if (dy != 0 && totalItemCount <= (lastVisibleItem + itemsToConsiderClose))
        {
            // Cannot call this method in a scroll callback.
            // Scroll callbacks might be run during a measure & layout
            // pass where you cannot change theRecyclerView data.
            // Any method call that might change the structure of the RecyclerView
            // or the adapter contents should be postponed to the next frame.
            recyclerView.post { onCloseToEnd() }
        }
    }
}
