package com.example.wikiarticles.recyclerview

import androidx.recyclerview.widget.DiffUtil

/**
 * An implementation of DiffUtil.Callback for single-type RecyclerView adapters (for example, ListAdapter<T>).
 */
class SingleTypeDiffCallback<T: Any>(private val oldDataList: List<T>,
                                     private val newDataList: List<T>,
                                     private val sameItems: T.(T) -> Boolean,
                                     private val sameContents: T.(T) -> Boolean) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldDataList.size

    override fun getNewListSize(): Int = newDataList.size

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return sameContents(oldDataList[oldItemPosition], newDataList[newItemPosition])
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return sameItems(oldDataList[oldItemPosition], newDataList[newItemPosition])
    }
}

fun <T: Any> ListAdapter<T, *>.notifyDataUpdatesViaDiffUtil(oldList: List<T>,
                                                            newList: List<T>,
                                                            sameItems: T.(T) -> Boolean,
                                                            sameContents: T.(T) -> Boolean) {
    DiffUtil
        .calculateDiff(
            SingleTypeDiffCallback(oldList, newList, sameItems, sameContents)
        )
        .dispatchUpdatesTo(this)
}