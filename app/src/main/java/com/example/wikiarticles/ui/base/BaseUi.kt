package com.example.wikiarticles.ui.base

import android.view.View

/**
 * An interface that represent UI for _BaseUiFragment_.
 */
interface BaseUi {
    val viewRoot: View
}

fun <T : View> BaseUi.findViewById(id:Int): T = viewRoot.findViewById(id)