package com.example.wikiarticles.ui.articles

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wikiarticles.api.WikiApiService
import com.example.wikiarticles.data.domain.WikiArticleData
import com.example.wikiarticles.location.LocationException
import com.example.wikiarticles.location.LocationTracker
import com.example.wikiarticles.ui.base.CoroutineScopedViewModel
import com.example.wikiarticles.utils.ResultOrError
import com.example.wikiarticles.utils.asError
import com.example.wikiarticles.utils.asResult
import kotlinx.coroutines.*
import java.io.IOException

/**
 * Loads and caches data for NearbyArticlesFragment.
 *
 * Loads articles only one time on first invocation to _getArticlesData()_.
 * Refresh feature has not been implemented for simplicity.
 */
class NearbyArticlesListViewModel(private val locationTracker: LocationTracker,
                                  private val apiService: WikiApiService) : CoroutineScopedViewModel() {

    private val articlesObservable by lazy {
                                          MutableLiveData<ResultOrError<List<WikiArticleData>, ArticlesLoadError>>()
                                              .also { liveData ->
                                                  uiScope.launch { liveData.value = loadArticles() }
                                              }
                                      }

    fun getArticlesData(): LiveData<ResultOrError<List<WikiArticleData>, ArticlesLoadError>> {
        return articlesObservable
    }

    private suspend fun loadArticles(): ResultOrError<List<WikiArticleData>, ArticlesLoadError> {
        return try {
            apiService
                .getNearbyArticlesAsync(locationTracker.getLastLocation())
                .asResult()
        } catch (e: LocationException) {
            ArticlesLoadError.LOCATION_UNAVAILABLE.asError()
        } catch (e: IOException) {
            ArticlesLoadError.NETWORK_ERROR.asError()
        }
    }
}

enum class ArticlesLoadError {
    LOCATION_UNAVAILABLE,
    NETWORK_ERROR
}