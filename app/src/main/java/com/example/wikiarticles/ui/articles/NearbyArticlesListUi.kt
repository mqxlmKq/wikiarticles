package com.example.wikiarticles.ui.articles

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.wikiarticles.R
import com.example.wikiarticles.data.domain.WikiArticleData
import com.example.wikiarticles.ui.base.InflatingContext
import com.example.wikiarticles.recyclerview.ListAdapter
import com.example.wikiarticles.recyclerview.notifyDataUpdatesViaDiffUtil
import com.example.wikiarticles.ui.articles.ArticlesLoadError.*
import com.example.wikiarticles.ui.base.BaseListUi
import com.example.wikiarticles.utils.showLongToast

class NearbyArticlesListUi(inflatingContext: InflatingContext,
                           onArticleClicked: (WikiArticleData) -> Unit) : BaseListUi(inflatingContext) {

    private val adapter: ListAdapter<WikiArticleData, NearbyArcticleViewHolder> = ListAdapter(
        createViewHolder  = {
            NearbyArcticleViewHolder(inflate(R.layout.list_item))
        },
        bindViewHolder    = { data ->
            titleView.text = data.title
        },
        itemClickListener = { _, data ->
            onArticleClicked(data)
        },
        updateNotifier = { oldList, newList, adapter ->
            adapter.notifyDataUpdatesViaDiffUtil(oldList,
                                                 newList,
                                                 sameItems = { pageId == it.pageId },
                                                 sameContents = { title == it.title })
        }
    )

    init {
        recyclerView.adapter = adapter
    }

    fun setArticlesList(data: List<WikiArticleData>) {
        adapter.dataList = data
    }

    fun showErrorMessage(error: ArticlesLoadError) {
        val errorResId = when (error) {
            LOCATION_UNAVAILABLE -> R.string.location_unavailable_error
            NETWORK_ERROR        -> R.string.network_error
        }

        viewRoot.context.showLongToast(errorResId)
    }
}

private class NearbyArcticleViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {
    val titleView: TextView = rootView.findViewById(R.id.title)
}