package com.example.wikiarticles.ui.articles

import android.content.Context
import androidx.lifecycle.Observer
import com.example.wikiarticles.R

import com.example.wikiarticles.getScreensNavigator
import com.example.wikiarticles.permissions.RuntimePermission
import com.example.wikiarticles.permissions.RuntimePermissionsRequesterCallback
import com.example.wikiarticles.permissions.RuntimePermissionsRequester
import com.example.wikiarticles.ui.base.InflatingContext
import com.example.wikiarticles.ui.base.BaseUiFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable

/**
 * This fragment represents list of nearby wiki articles (based on the last device's location).
 * The fragment requests location permission and then asynchronously loads wiki articles.
 *
 * The screen can show list of articles, loading state and error toast.
 * Pull-to-refresh feature has not been implemented for simplicity.
 */
class NearbyArticlesFragment : BaseUiFragment<NearbyArticlesListUi>(),
                               RuntimePermissionsRequesterCallback<Serializable?> {

    private val articlesViewModel: NearbyArticlesListViewModel by viewModel()

    override fun createUi(inflatingContext: InflatingContext): NearbyArticlesListUi {
        return NearbyArticlesListUi(inflatingContext,
                                    onArticleClicked = {
                                        getScreensNavigator()?.goToImagesListForArticleScreen(it.pageId)
                                    })
    }

    override fun bindUi(ui: NearbyArticlesListUi) {
        ui.showLoading()

        RuntimePermissionsRequester
            .from(this)
            .forPermission(RuntimePermission.LOCATION)
            .withDeniedToast(R.string.permission_request_location, R.string.permission_request_denied_location_to_search_articles)
            .requestWithArguments(null)
    }

    override fun onRuntimePermissionsGranted(context: Context, arguments: Serializable?) {
        articlesViewModel
            .getArticlesData()
            .observe(this, Observer { data ->
                data.ifResult {
                        ui.showContent()
                        ui.setArticlesList(it)
                    }
                    .ifError {
                        ui.showErrorMessage(it)
                    }
            })
    }

    companion object {

        // Empty factory method for consistency with ImagesListFragment
        fun newInstance() = NearbyArticlesFragment()
    }
}
