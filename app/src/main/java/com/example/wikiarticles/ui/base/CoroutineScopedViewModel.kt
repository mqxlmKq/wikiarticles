package com.example.wikiarticles.ui.base

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob

/**
 * This class manages coroutines in lifecycle-aware manner.
 * Active coroutines will survive configuration changes and will be automatically cancelled when user leaves current screen.
 */
abstract class CoroutineScopedViewModel : ViewModel() {
    private val viewModelJob = SupervisorJob()

    protected val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }
}