package com.example.wikiarticles.ui.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * This class is intended to encapsulates inflation details for current context (fragment, RecyclerView's adapter, etc.).
 */
class InflatingContext(private val inflater: LayoutInflater,
                       private val container: ViewGroup?) {
    val context: Context = inflater.context

    constructor(container: ViewGroup) : this(LayoutInflater.from(container.context), container)

    fun inflate(layoutId: Int): View {
        val view: View? = inflater.inflate(layoutId, container, false)

        return checkNotNull(view) { "Layout can't be inflated!" }
    }
}