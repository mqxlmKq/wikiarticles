package com.example.wikiarticles.ui.base

import android.view.View
import android.widget.ProgressBar
import androidx.recyclerview.widget.RecyclerView
import com.example.wikiarticles.R
import com.example.wikiarticles.utils.setGone
import com.example.wikiarticles.utils.setVisible

open class BaseListUi(inflatingContext: InflatingContext) : BaseUi {

    override val viewRoot: View = inflatingContext.inflate(R.layout.fragment_content_list)

    protected val recyclerView: RecyclerView = findViewById(R.id.recycler_view)
    protected val loadingView: ProgressBar = findViewById(R.id.loading_view)

    fun showLoading() {
        loadingView.setVisible()
        recyclerView.setGone()
    }

    fun showContent() {
        recyclerView.setVisible()
        loadingView.setGone()
    }
}