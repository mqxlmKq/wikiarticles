package com.example.wikiarticles.ui.images

import android.os.Bundle
import androidx.lifecycle.Observer
import com.example.wikiarticles.data.domain.WikiPageId
import com.example.wikiarticles.ui.base.BaseUiFragment
import com.example.wikiarticles.ui.base.InflatingContext
import com.example.wikiarticles.utils.getSerializableValue
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

/**
 * This fragment represents list of images for wiki article selected on previous screen.
 *
 * Images loading is paged, but this fact is not shown in UI (there is no progress item at the end of list).
 * Progress item has been omitted for simplicity. Progress item can be implemented via heterogeneous RecyclerView adapter.
 *
 * The screen can show list of media, loading state and error toast.
 * Pull-to-refresh feature has not been implemented for simplicity.
 */
class ImagesListFragment : BaseUiFragment<ImagesListUi>() {

    private val imagesViewModel: ImagesListViewModel by viewModel(parameters = { parametersOf(wikiPageId) })

    private val wikiPageId
        get() = requireNotNull(arguments).getSerializableValue<WikiPageId>(ARG_PAGE_ID)

    override fun createUi(inflatingContext: InflatingContext): ImagesListUi {
        return ImagesListUi(inflatingContext,
                            onCloseToEndOfList = {
                                imagesViewModel.tryLoadNextImagesPage()
                            })
    }

    override fun bindUi(ui: ImagesListUi) {
        ui.showLoading()

        imagesViewModel
            .getArticleImagesData()
            .observe(this, Observer { data ->
                data.ifResult {
                        ui.showContent()
                        ui.setImagesList(it)
                    }
                    .ifError {
                        ui.showNetworkErrorMessage()
                    }
            })
    }

    companion object {
        private const val ARG_PAGE_ID = "PAGE_ID"

        fun newInstance(webPageId: WikiPageId)
                = ImagesListFragment().apply {
                      arguments = Bundle().apply { putSerializable(ARG_PAGE_ID, webPageId) }
                  }
    }
}
