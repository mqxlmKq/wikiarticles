package com.example.wikiarticles.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment

/**
 * This base fragment is intended to extract UI-related code into separated class.
 * This UI-class must implement _BaseUi_ interface.
 *
 * Note: screen's logic also can be extracted to separated class to improve responsibilities separation.
 * This has not been implemented because all screens are quite simple, logic layer is almost empty.
 */
abstract class BaseUiFragment<UI : BaseUi> : Fragment() {

    protected lateinit var ui: UI
        private set

    final override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return createUi(InflatingContext(inflater, container))
                   .also {
                       ui = it
                   }
                   .viewRoot
    }

    @CallSuper
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindUi(ui)
    }

    abstract fun createUi(inflatingContext: InflatingContext): UI

    abstract fun bindUi(ui: UI)
}