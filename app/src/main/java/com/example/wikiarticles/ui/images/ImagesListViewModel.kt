package com.example.wikiarticles.ui.images

import androidx.lifecycle.LiveData
import com.example.wikiarticles.api.WikiApiService
import com.example.wikiarticles.data.domain.WikiArticleImageData
import com.example.wikiarticles.data.domain.WikiPageId
import com.example.wikiarticles.ui.base.CoroutineScopedViewModel
import com.example.wikiarticles.utils.ResultOrError

class ImagesListViewModel(private val apiService: WikiApiService,
                          private val wikiPageId: WikiPageId) : CoroutineScopedViewModel() {

    private val pageLoader by lazy {
                                  PagedImagesListLoader(wikiPageId, apiService)
                                      .also { it.tryLoadNextPageIfCan(uiScope) }
                              }

    fun getArticleImagesData(): LiveData<ResultOrError<List<WikiArticleImageData>, ArticleImagesLoadError>> {
        return pageLoader.getImagesData()
    }

    fun tryLoadNextImagesPage() {
        pageLoader.tryLoadNextPageIfCan(uiScope)
    }
}