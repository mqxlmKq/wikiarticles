package com.example.wikiarticles.ui.images

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.wikiarticles.R
import com.example.wikiarticles.data.domain.WikiArticleImageData
import com.example.wikiarticles.recyclerview.DetectCloseToEndOfListWhenScrolling
import com.example.wikiarticles.recyclerview.ListAdapter
import com.example.wikiarticles.recyclerview.notifyDataUpdatesViaDiffUtil
import com.example.wikiarticles.ui.base.BaseListUi
import com.example.wikiarticles.ui.base.InflatingContext
import com.example.wikiarticles.utils.showLongToast

class ImagesListUi(inflatingContext: InflatingContext,
                   onCloseToEndOfList: () -> Unit) : BaseListUi(inflatingContext) {

    private val adapter: ListAdapter<WikiArticleImageData, ArticleImageViewHolder> = ListAdapter(
        createViewHolder  = {
            ArticleImageViewHolder(inflate(R.layout.list_item))
        },
        bindViewHolder    = { data ->
            titleView.text = data.title
        },
        updateNotifier = { oldList, newList, adapter ->
            adapter.notifyDataUpdatesViaDiffUtil(oldList,
                                                 newList,
                                                 sameItems = { title == it.title },
                                                 sameContents = { this == it })
        }
    )

    init {
        recyclerView.adapter = adapter
        recyclerView.addOnScrollListener(
            DetectCloseToEndOfListWhenScrolling(recyclerView.layoutManager as LinearLayoutManager, onCloseToEndOfList)
        )
    }

    fun setImagesList(data: List<WikiArticleImageData>) {
        adapter.dataList = data
    }

    fun showNetworkErrorMessage() {
        viewRoot.context.showLongToast(R.string.network_error)
    }
}

private class ArticleImageViewHolder(rootView: View) : RecyclerView.ViewHolder(rootView) {
    val titleView: TextView = rootView.findViewById(R.id.title)
}