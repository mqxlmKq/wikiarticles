package com.example.wikiarticles.ui.images

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.wikiarticles.api.WikiApiService
import com.example.wikiarticles.data.domain.WikiArticleImageData
import com.example.wikiarticles.data.domain.WikiPageId
import com.example.wikiarticles.utils.ResultOrError
import com.example.wikiarticles.utils.asError
import com.example.wikiarticles.utils.asResult
import com.example.wikiarticles.utils.assertUiThread
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.io.IOException

/**
 * This class performs images list paginated loading for wiki article.
 * Method _tryLoadNextPageIfCan()_ can be safely called multiple times. Extra load calls will be ignored if loader
 * already loads data. When end of list is reached, further invocations of _tryLoadNextPageIfCan()_ will be ignored.
 *
 * If network error is occurred, loader will ignore further invocations of _tryLoadNextPageIfCan()_ (like in "end of list" state).
 *
 * This class assumes that all invocations to _tryLoadNextPageIfCan()_ will be from main thread.
 */
class PagedImagesListLoader(private val wikiPageId: WikiPageId,
                            private val apiService: WikiApiService) {

    private val imagesObservable = MutableLiveData<ResultOrError<List<WikiArticleImageData>, ArticleImagesLoadError>>()

    private var isLoadingNextPage: Boolean = false
    private var canLoadMore: Boolean = true
    private var continuationToken: String? = null

    fun tryLoadNextPageIfCan(uiScope: CoroutineScope) {
        assertUiThread()

        if (canLoadMore && !isLoadingNextPage) {
            isLoadingNextPage = true

            uiScope.launch {
                assertUiThread()

                try {
                    val page = apiService.getImagesListPageForArticleAsync(wikiPageId, continuationToken)

                    imagesObservable.value = imagesObservable.value?.mapResult { it + page.images }
                                             ?: page.images.asResult()

                    continuationToken = page.continuationToken
                } catch (e: IOException) {
                    imagesObservable.value = ArticleImagesLoadError.NETWORK_ERROR.asError()
                }

                canLoadMore = continuationToken != null && imagesObservable.value?.isError == false

                isLoadingNextPage = false
            }
        }
    }

    fun getImagesData(): LiveData<ResultOrError<List<WikiArticleImageData>, ArticleImagesLoadError>> {
        return imagesObservable
    }
}

enum class ArticleImagesLoadError {
    NETWORK_ERROR
}