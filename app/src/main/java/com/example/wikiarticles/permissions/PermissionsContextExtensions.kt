package com.example.wikiarticles.permissions

import android.content.Context
import android.content.pm.PackageManager
import androidx.core.content.ContextCompat

fun Context.hasRuntimePermission(permission: RuntimePermission): Boolean {
    return ContextCompat.checkSelfPermission(this, permission.systemName) == PackageManager.PERMISSION_GRANTED
}
