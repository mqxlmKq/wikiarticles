package com.example.wikiarticles.permissions.internal

import android.app.Dialog
import android.content.Context
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import android.os.Bundle
import androidx.appcompat.app.AlertDialog

/**
 * A dialog for RuntimePermissionsRequesterFragment (rationale and never ask again dialogs).
 *
 * This fragment dialog class expects that it starts from parent fragment (PermissionAlertDialogFragment.Builder do this),
 * otherwise dialog callbacks won't be called properly. Parent fragment must implement PermissionAlertDialogListener
 * interface, otherwise it would be treated as error.
 *
 * This class will invoke PermissionAlertDialogListener callbacks only if activity is attached and state is not saved.
 */
class PermissionAlertDialogFragment : DialogFragment() {
    companion object {
        private const val DIALOG_TITLE = "DIALOG_TITLE"
        private const val DIALOG_MESSAGE = "DIALOG_MESSAGE"
        private const val DIALOG_POSITIVE_TEXT = "DIALOG_POSITIVE_TEXT"
        private const val DIALOG_NEGATIVE_TEXT = "DIALOG_NEGATIVE_TEXT"
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val tag = requireNotNull(tag)
        val arguments = requireNotNull(arguments)
        val builder = AlertDialog.Builder(requireActivity())

        arguments.getString(DIALOG_TITLE)?.let { builder.setTitle(it) }
        arguments.getString(DIALOG_MESSAGE)?.let { builder.setMessage(it) }

        arguments.getString(DIALOG_POSITIVE_TEXT)
            ?.let {
                builder.setPositiveButton(it) { _, _ ->
                    withDialogButtonsListener { context ->
                        onPermissionDialogPositiveButtonPressed(context, tag)
                    }
                }
            }

        arguments.getString(DIALOG_NEGATIVE_TEXT)
            ?.let {
                builder.setNegativeButton(it) { _, _ ->
                    withDialogButtonsListener { context ->
                        onPermissionDialogNegativeButtonPressed(context, tag)
                    }
                }
            }

        return builder.setCancelable(false)
                      .create()
    }

    private fun withDialogButtonsListener(action: PermissionAlertDialogListener.(Context) -> Unit) {
        activity
            ?.takeUnless { isStateSaved }
            ?.let {
                (parentFragment as PermissionAlertDialogListener).action(it)

                dismiss()
            }
    }

    class Builder {
        private val arguments = Bundle()

        fun title(title: String): Builder {
            arguments.putString(DIALOG_TITLE, title)
            return this
        }

        fun message(message: String): Builder {
            arguments.putString(DIALOG_MESSAGE, message)
            return this
        }

        fun positiveText(positiveText: String): Builder {
            arguments.putString(DIALOG_POSITIVE_TEXT, positiveText)
            return this
        }

        fun negativeText(negativeText: String): Builder {
            arguments.putString(DIALOG_NEGATIVE_TEXT, negativeText)
            return this
        }

        fun showAllowingStateLoss(parentFragment: Fragment, fragmentTag: String): PermissionAlertDialogFragment {
            return PermissionAlertDialogFragment()
                       .also {
                           it.arguments = arguments
                           it.isCancelable = false

                           parentFragment
                               .childFragmentManager
                               .beginTransaction()
                               .add(it, fragmentTag)
                               .commitAllowingStateLoss()
                       }
        }
    }

    interface PermissionAlertDialogListener {
        fun onPermissionDialogPositiveButtonPressed(context: Context, dialogTag: String)
        fun onPermissionDialogNegativeButtonPressed(context: Context, dialogTag: String)
    }
}
