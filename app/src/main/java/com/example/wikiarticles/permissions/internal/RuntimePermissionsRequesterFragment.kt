package com.example.wikiarticles.permissions.internal

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.example.wikiarticles.permissions.RuntimePermissionsRequesterCallback
import com.example.wikiarticles.utils.getSerializableValue
import com.example.wikiarticles.utils.orElse
import com.example.wikiarticles.utils.showLongToast
import java.io.Serializable

/**
 * A child fragment, that handles runtime permissions requesting.
 *
 * Do not add this fragment directly! This class is a part of RuntimePermissionsRequester mechanics.
 * The fragment should be used via companion's requestPermissions() method.
 *
 * This fragment implements runtime permissions requesting flow (including rationale dialogs,
 * detailed rationale (never ask again) dialog and permissions denied toast). Also child fragment manages
 * configuration changes and state saving/restoring.
 *
 * Positive permissions request result will be delivered into RuntimePermissionsRequesterCallback<Args>.
 * First-priority callback target is parent fragment. If parent fragment is null, activity will be used.
 * If parent fragment or activity does not implement RuntimePermissionsRequesterCallback<Args> interface,
 * it will be treated as error (RuntimePermissionsRequester should guarantee correct callback implementation).
 */
class RuntimePermissionsRequesterFragment : Fragment(), PermissionAlertDialogFragment.PermissionAlertDialogListener {
    companion object {
        private const val RATIONALE_DIALOG_TAG = "RationaleDialogTag"
        private const val NEVER_ASK_AGAIN_DIALOG_TAG = "NeverAskAgainDialogTag"
        private const val MY_TAG = "RuntimePermissionsRequesterTag"

        private const val REQUESTER_CONFIG_KEY = "RequesterConfigKey"
        private const val REQUEST_ARGS_KEY = "RequestArgsKey"

        private const val REQUEST_CODE = 42

        fun <Args : Serializable?> requestPermissions(fragmentManager: FragmentManager,
                                                      config: PermissionsRequesterConfig,
                                                      requestArgs: Args) {
            val requester = RuntimePermissionsRequesterFragment().also {
                it.arguments = Bundle().also {
                    it.putSerializable(REQUEST_ARGS_KEY, requestArgs)
                    it.putSerializable(REQUESTER_CONFIG_KEY, config)
                }
            }

            fragmentManager
                .beginTransaction()
                .also { transaction ->

                    fragmentManager
                        .findFragmentByTag(MY_TAG)
                        ?.let {
                            transaction.remove(it)
                        }

                    transaction.add(requester, MY_TAG)
                }
                .commitNow()

            requester.requestRuntimePermissions()
        }
    }

    private val requesterConfig: PermissionsRequesterConfig
        get() = arguments!!.getSerializableValue(REQUESTER_CONFIG_KEY)

    private val requestArguments: Serializable?
        get() = arguments!!.getSerializable(REQUEST_ARGS_KEY)

    private fun requestRuntimePermissions() {
        activity?.let { activity ->
            requesterConfig
                .rationaleArgsProvider
                ?.takeIf { shouldShowRationaleDialog() }
                ?.let { showRationaleDialog(activity, it) }
                ?: doPermissionsRequest()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<out String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_CODE
            && requesterConfig.systemPermissionsArray.toSet() == permissions.toSet()
        ) {
            activity?.let { activity ->
                handlePermissionsRequestResult(activity,
                    permissionsGranted = grantResults.all { it == PackageManager.PERMISSION_GRANTED })
            }
        }
    }

    private fun handlePermissionsRequestResult(activity: Activity, permissionsGranted: Boolean) {
        if (permissionsGranted) {
            parentFragment
                .orElse { activity }
                // Assume that parent fragment/activity has already saved its state if this.isStateSaved is true.
                // Can't deliver result in this case.
                .takeUnless { isStateSaved }
                ?.let {
                    // Cast correctness should be guaranteed by RuntimePermissionsRequester.
                    @Suppress("UNCHECKED_CAST")
                    (it as RuntimePermissionsRequesterCallback<Serializable?>)
                        .onRuntimePermissionsGranted(activity, requestArguments)
                }
        } else {
            requesterConfig
                .neverAskAgainArgsProvider
                ?.takeUnless { shouldShowRationaleDialog() }
                ?.let { showNeverAskAgainDialog(activity, it) }
                ?: showPermissionDeniedMessage(activity)
        }
    }

    override fun onPermissionDialogPositiveButtonPressed(context: Context, dialogTag: String) {
        if (dialogTag == RATIONALE_DIALOG_TAG) {
            doPermissionsRequest()
        } else {
            openAppScreenInSystemSettings(context)
        }
    }

    override fun onPermissionDialogNegativeButtonPressed(context: Context, dialogTag: String) {
        showPermissionDeniedMessage(context)
    }

    private fun shouldShowRationaleDialog(): Boolean {
        return requesterConfig
            .permissions
            .any { shouldShowRequestPermissionRationale(it.systemName) }
    }

    private fun doPermissionsRequest() {
        requestPermissions(requesterConfig.systemPermissionsArray, REQUEST_CODE)
    }

    private fun showRationaleDialog(context: Context, dialogArgs: DialogArgsProvider) {
        PermissionAlertDialogFragment
            .Builder()
            .title(dialogArgs.getTitle(context))
            .message(dialogArgs.getMessage(context))
            .positiveText(getRationalePositiveText(context))
            .negativeText(getRationaleNegativeText(context))
            .showAllowingStateLoss(this, RATIONALE_DIALOG_TAG)
    }

    private fun showNeverAskAgainDialog(context: Context, dialogArgs: DialogArgsProvider) {
        PermissionAlertDialogFragment
            .Builder()
            .title(dialogArgs.getTitle(context))
            .message(dialogArgs.getMessage(context))
            .positiveText(getNeverAskAgainPositiveText(context))
            .negativeText(getNeverAskAgainNegativeText(context))
            .showAllowingStateLoss(this, NEVER_ASK_AGAIN_DIALOG_TAG)
    }

    private fun showPermissionDeniedMessage(context: Context) {
        requesterConfig
            .deniedMessageProvider
            ?.getMessage(context)
            ?.let { showLongToast(it) }
    }

    private val PermissionsRequesterConfig.systemPermissionsArray
        get() = permissions.map { it.systemName }.toTypedArray()

    private fun openAppScreenInSystemSettings(context: Context) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                         .setData(Uri.parse("package:" + context.packageName))
                         .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                         .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY)
                         .addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)

        context.startActivity(intent)
    }
}
