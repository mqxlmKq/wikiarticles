package com.example.wikiarticles.permissions.internal

import android.content.Context
import androidx.annotation.StringRes
import com.example.wikiarticles.permissions.RuntimePermission

import java.io.Serializable

class SinglePermissionRequestConfigurator<Args : Serializable?>(
    private val permission: RuntimePermission,
    private val requestPermission: (arguments: Args, config: PermissionsRequesterConfig) -> Unit
) {
    private var deniedToastMessageProvider: ToastMessageProvider? = null
    private var rationaleDialogArgsProvider: DialogArgsProvider? = null
    private var neverAskAgainDialogArgsProvider: DialogArgsProvider? = null

    fun withDeniedToast(
        @StringRes permissionResId: Int,
        @StringRes restrictedActionResId: Int
    ): SinglePermissionRequestConfigurator<Args> {
        assertNonZeroResources(permissionResId, restrictedActionResId)

        deniedToastMessageProvider =
            object : ToastMessageProvider {
                override fun getMessage(context: Context): String =
                    getDeniedMessage(context, permissionResId, restrictedActionResId)
            }

        return this
    }

    fun withRationaleDialog(
        @StringRes permissionResId: Int,
        @StringRes messageResId: Int
    ): SinglePermissionRequestConfigurator<Args> {
        assertNonZeroResources(permissionResId, messageResId)

        rationaleDialogArgsProvider =
            object : DialogArgsProvider {
                override fun getTitle(context: Context): String =
                    getPermissionRequestDialogTitle(context, permissionResId)

                override fun getMessage(context: Context): String = context.getString(messageResId)
            }

        return this
    }

    fun withNeverAskAgainDialog(
        @StringRes permissionResId: Int,
        @StringRes rationaleResId: Int,
        @StringRes permissionCapResId: Int
    ): SinglePermissionRequestConfigurator<Args> {
        assertNonZeroResources(permissionResId, rationaleResId, permissionCapResId)

        neverAskAgainDialogArgsProvider =
            object : DialogArgsProvider {
                override fun getTitle(context: Context): String =
                    getPermissionRequestDialogTitle(context, permissionResId)

                override fun getMessage(context: Context): String =
                    getNeverAskAgainDialogMessage(context, rationaleResId, permissionCapResId)
            }

        return this
    }

    fun requestWithArguments(arguments: Args) {
        requestPermission(
            arguments,
            PermissionsRequesterConfig(
                setOf(permission),
                deniedToastMessageProvider,
                rationaleDialogArgsProvider,
                neverAskAgainDialogArgsProvider
            )
        )
    }

    private fun assertNonZeroResources(@StringRes vararg stringResIds: Int) {
        stringResIds.forEach { require(it != 0) }
    }
}
