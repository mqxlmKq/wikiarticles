package com.example.wikiarticles.permissions

import android.Manifest

enum class RuntimePermission(val systemName: String) {
    CALL_PHONE    (Manifest.permission.CALL_PHONE),
    READ_CONTACTS (Manifest.permission.READ_CONTACTS),
    READ_STORAGE  (Manifest.permission.READ_EXTERNAL_STORAGE),
    WRITE_STORAGE (Manifest.permission.WRITE_EXTERNAL_STORAGE),
    CAMERA        (Manifest.permission.CAMERA),
    LOCATION      (Manifest.permission.ACCESS_COARSE_LOCATION)
}
