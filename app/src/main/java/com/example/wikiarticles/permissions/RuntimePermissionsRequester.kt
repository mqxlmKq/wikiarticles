package com.example.wikiarticles.permissions

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import com.example.wikiarticles.permissions.internal.RuntimePermissionsRequesterFragment
import com.example.wikiarticles.permissions.internal.SinglePermissionRequestConfigurator
import com.example.wikiarticles.utils.pairValues
import java.io.Serializable

/**
 * An entry point to request runtime permissions.
 *
 * To request permissions the client should do next steps:
 * 1) Client's activity/fragment must implement RuntimePermissionsRequesterCallback<Args> interface.
 * 2) Call RuntimePermissionsRequester.from() to start requester initialization.
 * 3) Configure requester (dialogs and toast to display).
 * 4) Request permissions.
 *
 * Rationale dialog, never ask again (detailed rationale) dialog and permissions denied toast are optional.
 * Request's arguments are also optional. If arguments are not required, the client should parametrize
 * callback interface with Serializable? type and pass null arguments into request.
 *
 * The RuntimePermissionsRequester's mechanics takes care about configuration changes, about state saving/restoring.
 * Request result will be delivered to the client only if it has attached context and state is not saved,
 * otherwise request result will be ignored. In this case the client needs to repeat permissions request.
 *
 * If the app already has requested permission, RuntimePermissionsRequester will immediately invoke
 * onRuntimePermissionsGranted() callback (in case if client has attached context and state is not saved).
 *
 * RuntimePermissionsRequester does not support concurrent permissions requests. If a new permissions request
 * is started, current active request will be lost (treated as cancelled).
 */
class RuntimePermissionsRequester<Args : Serializable?> private constructor(
    private val contextProvider: () -> Context?,
    private val fragmentManagerProvider: () -> FragmentManager,
    private val permissionsCallback: RuntimePermissionsRequesterCallback<Args>
) {
    companion object {

        fun <T, Args : Serializable?> from(fragment: T): RuntimePermissionsRequester<Args>
                where T : Fragment,
                      T : RuntimePermissionsRequesterCallback<Args> {

            return RuntimePermissionsRequester(fragment::getActivity,
                                             fragment::getChildFragmentManager,
                                             fragment)
        }

        fun <T, Args : Serializable?> from(activity: T): RuntimePermissionsRequester<Args>
                where T : FragmentActivity,
                      T : RuntimePermissionsRequesterCallback<Args> {

            return RuntimePermissionsRequester({ activity },
                                             activity::getSupportFragmentManager,
                                             activity)
        }
    }

    fun forPermission(permission: RuntimePermission): SinglePermissionRequestConfigurator<Args> {
        return SinglePermissionRequestConfigurator(permission) { arguments, config ->
            fragmentManagerProvider()
                .takeUnless { it.isStateSaved }
                .pairValues(contextProvider())
                ?.let { (fragmentManager, context) ->
                    if (context.hasRuntimePermission(permission)) {
                        // Requested permission has been already granted, immediately invoke callback.
                        permissionsCallback
                            .onRuntimePermissionsGranted(context, arguments)
                    } else {
                        // Delegate runtime permissions request to child fragment.
                        RuntimePermissionsRequesterFragment
                            .requestPermissions(fragmentManager, config, arguments)
                    }
                }
        }
    }
}

/**
 * Implement this interface to use with RuntimePermissionsRequester.
 * Permissions request result will be delivered into onRuntimePermissionsGranted().
 */
interface RuntimePermissionsRequesterCallback<Args : Serializable?> {
    fun onRuntimePermissionsGranted(context: Context, arguments: Args)
}
