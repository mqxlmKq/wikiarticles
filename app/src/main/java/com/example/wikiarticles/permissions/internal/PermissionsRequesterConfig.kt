package com.example.wikiarticles.permissions.internal

import android.content.Context
import java.io.Serializable
import com.example.wikiarticles.permissions.RuntimePermission

class PermissionsRequesterConfig(
    permissions: Set<RuntimePermission>,
    val deniedMessageProvider: ToastMessageProvider?,
    val rationaleArgsProvider: DialogArgsProvider?,
    val neverAskAgainArgsProvider: DialogArgsProvider?
) : Serializable {

    private val serializablePermissions = HashSet(permissions)

    val permissions: Set<RuntimePermission>
        get() = serializablePermissions
}

interface ToastMessageProvider : Serializable {
    fun getMessage(context: Context): String
}

interface DialogArgsProvider : Serializable {
    fun getTitle(context: Context): String
    fun getMessage(context: Context): String
}
