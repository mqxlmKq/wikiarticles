package com.example.wikiarticles.permissions.internal

import android.content.Context
import com.example.wikiarticles.R

fun getPermissionRequestDialogTitle(
    context: Context,
    permissionResId: Int
): String {
    return context.getString(
        R.string.permission_request_title_use,
        context.getString(permissionResId)
    )
}

fun getRationalePositiveText(context: Context): String {
    return context.getString(R.string.permission_request_rationale_positive)
}

fun getRationaleNegativeText(context: Context): String {
    return context.getString(R.string.permission_request_rationale_negative)
}

fun getDeniedMessage(
    context: Context,
    permissionResId: Int,
    restrictedActionResId: Int
): String {
    return context.getString(
        R.string.permission_request_denied,
        context.getString(permissionResId),
        context.getString(restrictedActionResId)
    )
}

fun getNeverAskAgainPositiveText(context: Context): String {
    return context.getString(R.string.permission_request_never_ask_again_positive)
}

fun getNeverAskAgainNegativeText(context: Context): String {
    return context.getString(R.string.permission_request_never_ask_again_negative)
}

fun getNeverAskAgainDialogMessage(
    context: Context,
    rationaleResId: Int,
    permissionCapitalizedResId: Int
): String {
    val rationaleText = context.getString(rationaleResId)
    val neverAskAgainText = context.getString(
                                R.string.permission_request_never_ask_again_message,
                                context.getString(permissionCapitalizedResId)
                            )

    return "$rationaleText\n\n$neverAskAgainText"
}