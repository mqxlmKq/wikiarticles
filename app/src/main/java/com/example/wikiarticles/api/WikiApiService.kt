package com.example.wikiarticles.api

import com.example.wikiarticles.api.retrofit.WikiApi
import com.example.wikiarticles.data.domain.*
import com.example.wikiarticles.data.mapping.toImagesPageData
import com.example.wikiarticles.data.mapping.toWikiArticlesList

/**
 * An entry point for all interactions with wiki API.
 */
class WikiApiService(private val api: WikiApi) {

    suspend fun getNearbyArticlesAsync(location: LocationData): List<WikiArticleData> {
        return api.getNearbyArticlesAsync(location.asApiParameter())
                  .await()
                  .let { it.toWikiArticlesList() }
    }

    suspend fun getImagesListPageForArticleAsync(pageId: WikiPageId,
                                                 continuationToken: String? = null): WikiArticleImagesPageData {
        return api.getImagesPageForArticleAsync(pageId.asInt, continuationToken)
                  .await()
                  .let { it.toImagesPageData() }
    }

    private fun LocationData.asApiParameter() = "$latitude|$longitude"
}