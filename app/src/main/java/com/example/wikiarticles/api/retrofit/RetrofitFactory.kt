package com.example.wikiarticles.api.retrofit

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://en.wikipedia.org/w/api.php/"

fun createRetrofit(): Retrofit
        = Retrofit.Builder()
                  .baseUrl(BASE_URL)
                  .addConverterFactory(GsonConverterFactory.create())
                  .addCallAdapterFactory(CoroutineCallAdapterFactory())
                  .build()