package com.example.wikiarticles.api.retrofit

import com.example.wikiarticles.data.api.NearbyArticlesApiResponse
import com.example.wikiarticles.data.api.PagedImagesListApiResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface WikiApi {

    @GET("?action=query&list=geosearch&gsradius=10000&gslimit=50&format=json")
    fun getNearbyArticlesAsync(@Query("gscoord") coordinates: String): Deferred<NearbyArticlesApiResponse>

    @GET("?action=query&format=json&prop=images")
    fun getImagesPageForArticleAsync(@Query("pageids") pageId: Int,
                                     @Query("imcontinue") continuationToken: String?): Deferred<PagedImagesListApiResponse>
}